from aws_deploy import application


class TestRun:
    def setup_class(cls):
        cls.run = application.Run()

    def test_return_5(self):
        assert 5 == self.run.return_5()
