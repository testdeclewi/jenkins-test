#!/usr/bin/env bash
OLD_DIR=`pwd`
cd "$(dirname "$0")"
pip install -r ../requirements.txt
screen -S pyapp -d -m python ../aws_deploy/application.py
cd ${OLD_DIR}
