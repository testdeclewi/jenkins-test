from setuptools import setup

setup(name='test aws deploy',
      version='0.1',
      description='simple testing code',
      author='Wim De Clercq',
      author_email='wim.declercq@xt-i.com',
      packages=['aws_deploy'])